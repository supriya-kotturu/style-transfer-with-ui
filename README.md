# style-transfer-with-UI

Creating UI for DL project "neural algorithm for style transfer", where a new image is generated when content and style images are fed to the VGG16 network. The goal is to provide a user interface, where a user can select or upload the images